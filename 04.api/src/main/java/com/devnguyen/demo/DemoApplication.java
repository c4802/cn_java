package com.devnguyen.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	@Autowired
	private JdbcTemplate jdbc;

	public static void main(String[] args) {
		System.out.println("URL: http://localhost:8080");
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
        // String sql = "INSERT INTO nguoi_dung(name, age) VALUES ('Nguyễn Văn A', 20);";
         
        // int rows = jdbc.update(sql);
        // if (rows > 0) {
        //     System.out.println("A new row has been inserted.");
        // }
    }

}
